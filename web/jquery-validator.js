/**
 @info:based on jquery.js
 @source:jquery-validator.js
 @author:dandy1992
 @date:2014-01-11
 **/
(function($){
    $.extend({
        match:{
            'money':{
                regex:/^(([1-9]\d{0,9})|0)(\.\d{1,2})?$/,
                words:'必须是金钱格式(可带二位小数)'
            },
            'digits':{
                regex: /^\d+$/,
                words:'必须为数字'
            },
            'year':{
                regex: /^\d{4}$/,
                words:'必须是4位数的年份'
            },
            'month':{
                regex:/^0[1-9]$|^1[0-2]$|^[1-9]$/,
                words:'必须是<=12的月份'
            },
            'email':{
                regex:/^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/,
                words:'不符合邮箱格式'
            },
            'phone':{
                regex:/^(1(([35][0-9])|(47)|[8][01236789]))\d{8}$/,
                words:'必须是手机号码格式'
            },
            'qq':{
                regex:/^[1-9]\d{4,8}$/,
                words:'不是正确的qq号码'
            },
            'ip':{
                regex:/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/,
                words:'不是合法的ip地址'
            },
            'telephone':{
                regex:/^(([0\+]\d{2,3}-)?(0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/,
                words:'不符合电话号码的格式'
            },
            'passwd':{
                regex:/^[a-zA-Z][a-zA-Z0-9_]{4,15}$/,//密码合法,字母开头,下划线数字字母 共5-16长度
                words:'不是合法的密码格式'
            },
            'chinese':{
                regex:/^[\u4e00-\u9fa5]+$/,
                words:'不是中文格式'
            },
            'id':{ //特指身份证
                regex:/^(\d{14}|\d{17})(\d|[xX])$/,
                words:'不是有效的身份证号码'
            },
            //弱验证
            'require':{
                regex:/[\S\s]+/,
                words:'不能为空'
            }

        },
        Validators:{
            check: function(n,s){
                if(!$.match[n]){
                    throw (new Error('illegal validator name','validator error'));
                }
                if(!$.match[n].regex) return false;
                return $.match[n].regex.test(s);
            },

            validate:function(){
                var r = this.vs;
                var s = this.values;
                var w = this.words;
                var flag = true;
                $.each(r,function(index,value){
                    if(!$.Validators.check(value,s[index])){
                        alert(w[index] + $.match[value].words);
                        flag = false;
                        return false;
                    }
                });
                return flag;
            },
            reset:function(){
                this.vs=[];
                this.values=[];
                this.words=[];
            },
            vs:[],
            values:[],
            words:[],
        }



    });

    $.fn.extend({
        addValidator:function(validator,word){
            if(typeof validator == 'string'){
                $.Validators.vs.push(validator);
                $.Validators.values.push($.trim(this.val()));
                $.Validators.words.push(word);
            }else if(typeof validator == 'object'){
                var t = validator;
                if(!t.name || !t.regex || !t.words){
                    throw (new Error('not a valid validator','validator error'));
                }
                var obj = {regex:t.regex,words:t.words};
                $.match[t.name] = obj;
                this.addValidator(t.name,word);
            }else{
                throw (new Error('not a valid validator','validator error'));
            }
            return this;
        }
    });

})(jQuery);