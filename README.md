#jquery-validator

   也许你经常用正则来验证表单,也许你是很精通正则表达式,但是你可能都会厌倦重复的工作,
假如有一天你写出了一个很复杂的正则,你觉得它很通用,你会精通去copy,paste这个正则还是
拓展自己的js验证框架呢?

   jquery-validator正是为此而拓展的一个简单表单验证框架,使用者可以使用内置的验证器和
加入自己的验证器


先看一段常用的验证代码:

    $('#form').onSubmit(function(){
        
        var phone = $('#phone').val().trim()
        if(!phone || !/^(1(([35][0-9])|(47)|[8][01236789]))\d{8}$/.test(phone)){
            alert('不是合法的手机号码');
            return false;
        }
        
        var passwd = $('#passwd').val().trim()
        if(!passwd || !/^[a-zA-Z][a-zA-Z0-9_]{4,15}$/.test(passwd)){
            alert('不是合法的密码格式');
            return false;
        }
        ....
    
    });


   js代码中充斥着各种if,alert,以及return,并且表单的val()过长,你不得不另起一
行声明他的value值,这在一定程度上影响了代码的美观


如果有了jquery-validator.js,那么你可以这样写:

    $('#form').onSubmit(function(){
        
        //内置的2个验证器,支持链式表达,返回调用对象
       $('#phone').addValidator('phone','手机输入');
       $('#passwd').addValidator('passwd','密码');
        ....
        //重写已有的 或者添加新的验证器,只需要一个json格式
        var myValidator = {'name':'passwd','regex':/^[a-zA-Z][a-zA-Z0-9_]{4,15}$/,words:'不是合法的密码格式'};
        $('#passwd').addValidator('passwd','密码');
        //最后调用验证方法即可
        if(!$.Validators.validate()){
            return false;
        }
    
    });
    
 // 欢迎留下宝贵的批评指正意见,如果您觉得还不错,那就请我喝杯咖啡吧,http://me.alipay.com/dandy1992 